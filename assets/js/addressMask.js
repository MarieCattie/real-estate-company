let reg = /ул\.\s.+?\s([0-9\s]){1,4}-([0-9\s]){1,4}/
$('#sendForm').on('submit', function (e) {
    if(!reg.test($('#address').val())) {
        e.preventDefault()
        alert('Неверный формат адреса. \n Правильный формат: ул. Наименование №дома-№улицы \n Например: ул. Иванова 23-80');
    }
})