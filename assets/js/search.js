$(document).ready(function () {
    let cards = document.querySelectorAll('.realestate-card');
    $('#search-form').on('submit', function (e) {
        e.preventDefault();
        let cardsArr = [];
        let searchNumber = $('#search-house').val();
        for(let i = 0; i < cards.length; i++) {

            cardsArr.push(cards[i]);
        }
        if (searchNumber == ''){
            cardsArr.forEach(function (card) {
                $('#realestate-container').append(card)
            })
        }
        else {
            $('#realestate-container').html('')
            cardsArr.filter(function (card) {
                let address = card.querySelector('.card-address').textContent;
                let addressArr = address.split(' ');
                let houseNumber = parseInt(addressArr[addressArr.length - 1].split('-')[0]);
                if (parseInt(houseNumber) == parseInt(searchNumber)) {
                    return card
                }
            }).forEach(function (node) {
                $('#realestate-container').append(node)
            })
        }

    })
})

