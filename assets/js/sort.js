$(document).ready(function () {
    let cards = document.querySelectorAll('.realestate-card');
    let parent = document.querySelector('#realestate-container');

    $('#ascending').on('click', function () {
        let sorted = [...cards].sort(function (a, b) {
            return a.querySelector('.price-count').textContent - b.querySelector('.price-count').textContent;
        })
        parent.innerHTML = '';
        for(let elem of sorted) {
            parent.appendChild(elem)
        }
    })

    $('#descending').on('click', function () {
        let sorted = [...cards].sort(function (a, b) {
            return b.querySelector('.price-count').textContent - a.querySelector('.price-count').textContent;
        })
        parent.innerHTML = '';
        for(let elem of sorted) {
            parent.appendChild(elem)
        }
    })
})