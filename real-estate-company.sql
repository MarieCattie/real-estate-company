-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 05 2022 г., 17:50
-- Версия сервера: 8.0.24
-- Версия PHP: 8.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `real-estate-company`
--

-- --------------------------------------------------------

--
-- Структура таблицы `real_estate`
--

CREATE TABLE `real_estate` (
  `id` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` text,
  `address` text,
  `price` varchar(11) DEFAULT NULL,
  `description` text,
  `is_actual` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `real_estate`
--

INSERT INTO `real_estate` (`id`, `title`, `image`, `address`, `price`, `description`, `is_actual`) VALUES
(1, 'Объект 1', '21593.jpeg', 'ул. Мамадышская 45-78', '7500000', '9-ый дом, 180 кв. метров, двухуровневая', 0),
(2, 'Объект 2', '55928.jpeg', 'ул. Гагарина 56-56', '3900000', 'ул. Азата Аббасова, д. 11, Казань', 1),
(3, 'Объект 3', '87977.jpeg', 'ул. Ленинградская 97-01', '4500000', 'частный дом из сруба, 100 кв.метров', 0),
(4, 'Объект 4', '66999.jpeg', 'ул. Галимджана Баруди 59-78', '3200000', 'Кирпичный дом, 80 кв. метров', 0),
(5, 'Объект 5', '78066.jpeg', 'ул. 50 лет Победы 24-32', '2500000', 'Панельный дом, 45 кв. метров, с двумя лоджиями', 1),
(6, 'Объект 6', '99987.jpeg', 'ул. Аббасова 11-22', '3900000', 'Продается 2-комн. кв., 64 м2, 9/19 этаж', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(50) DEFAULT NULL,
  `password` text,
  `is_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `is_admin`) VALUES
(1, 'admin', '$2y$10$.ZDWJtQWA4idWLL9qAmtZ.wIbIDgtRseCW2Lve3K0..2boQoHCIzG', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `real_estate`
--
ALTER TABLE `real_estate`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `real_estate`
--
ALTER TABLE `real_estate`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
