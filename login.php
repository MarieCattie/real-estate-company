<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <title>Вход администратора</title>
</head>
<body>
<div class="container py-5 d-flex align-items-center flex-column">
    <h1 class="h1 text-center mt-3 mb-3">Вход</h1>
    <form method="post" class="row align-items-center d-flex flex-column w-50" action="app/forms/login.php">
        <div class="form-floating mb-3">
            <input name="login" type="text" class="form-control" id="login" placeholder="admin">
            <label class="px-4" for="login">Логин</label>
        </div>
        <div class="form-floating mt-3">
            <input name="password" type="password" class="form-control" id="pass" placeholder="password">
            <label class="px-4" for="pass">Пароль</label>
        </div>
        <button type="submit" class="btn btn-primary w-50 mt-3">Войти</button>
    </form>
</div>

</body>
</html>