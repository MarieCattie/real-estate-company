<?php
session_start();
require "init.php";
require "app/connection.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <style>
        .preview {
            width: 400px;
            height: 325px;
        }
        .preview img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
    <title>Обновить</title>
</head>
<body>
<!-- header -->
<nav class="navbar navbar-light bg-light">
    <div class="container justify-content-between align-items-center">
        <a href="index.php" class="btn btn-outline-primary">Назад на главную</a>
    </div>
</nav>
<!--  ||header -->
<div class="container d-flex  align-items-center justify-content-center mb-3">
    <form action="app/forms/edit.php" method="post" enctype="multipart/form-data" class="row g-3 mt-3 justify-content-center" style="width: 70%;" id="sendForm">
        <h1 class="h1 text-center mb-0">Обновить</h1>
        <h5 class="text-second text-center h5 mt-1 mb-5">объект недвижимости</h5>
        <?php
        $item = new \app\controllers\RealEstate($_GET['id'], $config);
        ?>
        <input type="hidden" name="MAX_FILE_SIZE" value="200000000">
        <input type="hidden" name="id" value="<?=$item->id?>">
        <div class="col-6">
            <label for="title" class="form-label">Наименование</label>
            <input type="text" value="<?=$item->title?>" class="form-control" id="title" name="title" placeholder="Объект 1">
        </div>
        <div class="col-6">
            <label for="image" class="form-label">Изображение</label>
            <input type="file" class="form-control" id="image" name="image">
        </div>
        <div class="col-6">
            <label for="address" class="form-label">Адрес</label>
            <input type="text" value="<?=$item->address?>" class="form-control" id="address" name="address" placeholder="ул. Наименование №дома-№кв" ">
        </div>
        <div class="col-6">
            <label for="price" class="form-label">Цена</label>
            <input type="number" value="<?=$item->price?>" class="form-control" id="price" name="price" placeholder="1000000">
        </div>
        <div class="col-md-12">
            <label for="description" class="form-label">Описание</label>
            <textarea class="form-control" name="description" id="description" cols="10" rows="10" placeholder="Панельный дом, 45 кв. метров, с двумя лоджиями"><?=$item->description?></textarea>
        </div>
        <div class="col-12">
            <label for="is_actual" class="form-label">Статус</label>
            <select id="is_actual" name="is_actual" class="form-select">
                <option value="0" <?= ($item->is_actual == 0)? 'selected' : ''?>>Не актуально</option>
                <option value="1" <?= ($item->is_actual == 1)? 'selected' : ''?>>Актуально</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary col-6 align-self-center">Обновить</button>
    </form>
    <div class="mx-5 bg-dark preview">
        <?php if($item->image !== ''): ?>
            <img src="assets/img/userfiles/<?=$item->image?>" alt="">
        <?php else: ?>
            <img src="assets/img/preview.png" alt="">
        <?php endif; ?>
    </div>
</div>
<!-- ||form -->
<script src="assets/js/jquery.min.js"></script>
<!--<script src="assets/js/jquery.inputmask.bundle.min.js"></script>-->
<script src="assets/js/addressMask.js"></script>
</body>
</html>
