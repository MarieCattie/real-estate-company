<?php
session_start();
require "init.php";
require "app/connection.php";
if(isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
else {
    $user = false;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Агенство недвижимости</title>
</head>
<body>
<div class="container-fluid">
    <!-- header -->
    <nav class="navbar navbar-light bg-light">
        <div class="container justify-content-between align-items-center">
            <?php if($user): ?>
                <a href="add.php" class="btn btn-primary">Добавить новый объект недвижимости</a>
            <?php endif; ?>

            <div class="d-flex align-items-center">
                <p class="color-success">Добро пожаловать,
                    <?php if($user): ?>
                        <span class="text-primary"><?= $user['login'] ?></span></p>
                        <a href="app/forms/logout.php" class="btn btn-outline-danger mx-3">Выйти</a>
                    <?php else: ?>
                        <span class="text-primary">Гость</span></p>
                        <a href="login.php" class="btn btn-outline-primary mx-3">Войти</a>
                    <?php endif; ?>
            </div>
        </div>
    </nav>
    <!--  ||header -->
    <div class="container pt-2">
        <div class="d-flex mt-5 pt-5 justify-content-between">
            <form method="post" class="d-flex" id="search-form">
                <input type="hidden" name="user" id="current-user" value="<?php ($user)? $user['id']: ''?>">
                <input class="form-control me-2 col-6" type="search" placeholder="Найти по номеру дома"
                       aria-label="Search" name="house" id="search-house">
                <button class="btn btn-outline-success" type="submit">Найти</button>
            </form>
            <div class="d-flex align-items-center">
                <p class="text-secondary me-2">Сортировка по цене</p>
                <div class="d-flex">
                    <div class="me-3">
                        <input type="radio" class="btn-check" name="options" id="ascending" autocomplete="off">
                        <label class="btn btn-sort" for="ascending">по возрастанию</label>
                    </div>
                    <div>
                        <input type="radio" class="btn-check" name="options" id="descending" autocomplete="off">
                        <label class="btn btn-sort" for="descending">по убыванию</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="row mt-4" id="realestate-container">

        <?php
        if(!$user) {
            //для неавторизованных пользователей выводить только актуальные
           $result = \app\controllers\RealEstate::getActual($db);
        }
        else {
            //для авторизованных выводить все
            $result = \app\controllers\RealEstate::all($db);
        }

        foreach ($result as $item): ?>
            <!-- card -->
            <div class="col-lg-4 col-md-6 col-sm-12 realestate-card">
                <div class="card">
                    <?php if($item['image'] == ''): ?>
                    <img src="assets/img/preview.png" class="card-img-top" alt="...">
                    <?php else: ?>
                    <img src="assets/img/userfiles/<?= $item['image'] ?>" class="card-img-top" alt="...">
                    <?php endif; ?>
                    <div class="card-body d-flex flex-column justify-content-between">
                        <h5 class="card-title"><?= $item['title'] ?></h5>
                        <p class="card-text text-secondary  mb-2 card-address"><?= $item['address'] ?></p>
                        <p class="card-text text-justify"><?= $item['description'] ?></p>
                        <div class="d-flex justify-content-between mt-3">
                            <a href="#" class="btn btn-primary">Перейти</a>
                            <?php if(!$item['is_actual']): ?>
                            <p class="text-danger">Не актуально</p>
                            <?php endif; ?>
                            <p class="text-end price"><span class="price-count"><?= $item['price'] ?></span> &#8381;</p>
                        </div>
                        <div class="d-grid gap-2 mt-2">
                        <?php if($user): ?>
                            <a href="edit.php?id=<?=$item['id']?>" class="btn btn-success mt-3">Редактировать</a>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ||card -->
        <?php endforeach; ?>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/search.js"></script>
<script src="assets/js/sort.js"></script>
</body>
</html>
