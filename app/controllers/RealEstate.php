<?php


namespace app\controllers;

use app\database\Sql;

class RealEstate extends Sql
{
    public $id;
    public $title;
    public $address;
    public $price;
    public $description;
    public $is_actual;
    public $image;

    public function __construct($id, $config)
    {
        parent::__construct($config);
        $sql = "SELECT * FROM real_estate WHERE id = :id";
        $result = self::getRow($sql, ['id' => $id]);
        $this->id = $id;
        $this->title = $result['title'];
        $this->address = $result['address'];
        $this->price = $result['price'];
        $this->description = $result['description'];
        $this->is_actual = $result['is_actual'];
        $this->image = $result['image'];
    }
    public static function all(Sql $sql)
    {
        $query = "SELECT * FROM real_estate ORDER BY id DESC";
        $result = $sql->getArray($query);
        return $result;
    }
    public static function getActual(Sql $sql)
    {
        $query = "SELECT * FROM real_estate WHERE is_actual = 1";
        $result = $sql->getArray($query);
        return $result;
    }
    public static function loadFile($filename)
    {
        $name = rand(10000, 99999);
        $ex = explode('/', $_FILES[$filename]['type']);
        $filetype = $ex[1];
        $file = $name . '.' . $filetype;
        if($_FILES[$filename]['type'] == 'image/png' || $_FILES[$filename]['type'] == 'image/jpg' || $_FILES[$filename]['type'] == 'image/jpeg')
        {
            move_uploaded_file($_FILES[$filename]['tmp_name'], '../../assets/img/userfiles/' . $file);
            return $file;
        }
        return false;
    }

}