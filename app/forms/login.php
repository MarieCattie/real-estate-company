<?php
session_start();
require "../../init.php";
require "../connection.php";
$login = $_POST['login'];
$password = $_POST['password'];
$sql = "SELECT * FROM users WHERE login = :login";
$result = $db->getRow($sql, ['login' => $login]);
if(count($result) == null)
{
    exit('Пользователь не найден');
}
if(!password_verify($password, $result['password']))
{
    exit('Пароль неверный');
}
$_SESSION['user'] = [
  'id' => $result['id'],
  'login' => $result['login'],
  'is_admin' => $result['is_admin']
];
header("Location: ../../index.php");