<?php
require "../../init.php";
require "../connection.php";
if(!($upload = \app\controllers\RealEstate::loadFile('image')))
{
    exit('Неверный тип файла');
}
$params = [
    'title' => $_POST['title'],
    'address' => $_POST['address'],
    'price' => $_POST['price'],
    'description' => $_POST['description'],
    'is_actual' => $_POST['is_actual'],
    'image' => $upload
];
$sql = "INSERT INTO real_estate (title, address, price, description, is_actual, image) VALUES(:title, :address, :price, :description, :is_actual, :image)";
$db->execute($sql, $params);
header("Location: ../../index.php");

