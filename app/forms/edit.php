<?php
require "../../init.php";
require "../connection.php";
header("Location: ../../index.php");
$params = [
    'id' => $_POST['id'],
    'title' => $_POST['title'],
    'address' => $_POST['address'],
    'price' => $_POST['price'],
    'description' => $_POST['description'],
    'is_actual' => $_POST['is_actual'],
];
$sql = "UPDATE real_estate SET title = :title, address = :address, price = :price, description = :description, is_actual = :is_actual WHERE id = :id";
$db->execute($sql, $params);

//Проверка, загружено ли изображение
if($_FILES['image']['name'] == '') { exit; }

if(!($upload = \app\controllers\RealEstate::loadFile('image', $db)))
{
    exit('Неверный тип файла');
}
$query = "UPDATE real_estate SET image = :image WHERE id = :id";
$db->execute($query, ['image' => $upload, 'id' => $_POST['id']]);

