<?php


namespace app\database;

use PDO;

class Sql {
    //подключение к БД
    public $link;

    public function __construct($config) {
        $this->connect($config);
    }

    private function connect($config) {
        $dsn = 'mysql:host='.$config['host'].';dbname='.$config['db_name'].'';
        $this->link = new PDO($dsn, $config['username'], $config['password']);
        return $this;
    }
    //выполняет sql-запрос
    public function execute($sql, $params = null)
    {
        $smt = $this->link->prepare($sql);
        return $smt->execute($params);
    }

    public function getRow($sql, $params = null) {
        $smt = $this->link->prepare($sql);
        $smt->execute($params);
        $result = $smt->fetch(PDO::FETCH_ASSOC);
        if(!$result) {
            return [];
        }
        return $result;
    }

    public function getArray($sql, $params = null) {
        $smt = $this->link->prepare($sql);
        $smt->execute($params);
        $result = $smt->fetchAll(PDO::FETCH_ASSOC);
        if(!$result) {
            return [];
        }
        return $result;
    }
}